#ifndef _CLIENT_H_
#define _CLIENT_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/signal.h>
#include <sys/wait.h>

#include "../lib/fon.h"   		/* primitives de la boite a outils */
#include "communication.h"

#define SERVICE_DEFAULT "1111" // Port par défaut
#define SERVER_DEFAULT  "127.0.0.1" // Adresse par défaut

#define BUFFER_SIZE     32 // Taille du buffer
#define PSEUDO_SIZE     10 // Taille du pseudp

/**
 * @brief Initialise le client
 * 
 * @param server String adresse du server
 * @param service String port à utiliser
 */
void start(char *server, char *service);

/**
 * @brief Affiche l'aide de l'application
 */
void print_help();

/**
 * @brief Lance l'exécution du client
 * 
 * @param socket Int socket du client
 * @param pseudo String pseudo de l'utilisateur
 */
void run(int socket, char *pseudo);

/**
 * @brief Nettoye l'entrée standard
 * 
 */
void clear_stdin();

/**
 * @brief Affiche le message reçu
 * 
 * @param socket Int Socket du client
 * @param buffer String buffer avec le message reçu
 */
void display(int socket, char *buffer);

/**
 * @brief Envoie un message au serveur (instruction et données)
 * 
 * @param socket int Socket du clien
 * @param line string Ligne de commande de l'utilisateur
 * @param pseudo string pseudo de l'utilisateur
 */
void send_server(int socket, char *line, char *pseudo);

/**
 * @brief Découpe une chaine de caractères
 * 
 * @param string La chaine à découper
 * @param begin  Position à laquelle commence la découps
 * @param length Taille de la découpe
 * @return char* Le résultat de la découpe
 */
char *substr(char *string, int begin, int length);

#endif //_CLIENT_H_