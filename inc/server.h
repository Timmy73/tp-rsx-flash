#ifndef _SERVER_H_
#define _SERVER_H_

#include <stdio.h>
#include <stdlib.h>
#include <sys/signal.h>
#include <sys/wait.h>

#include "../lib/fon.h"     		/* Primitives de la boite a outils */
#include "../lib/flash.h"
#include "communication.h"

#define SERVICE_DEFAUT "1111" // Port par défaut
#define NB_CONNEXION_DEFAUT 10 // Nombre de connexions max

// Strucure de données pour la gestion du serveur
typedef struct server_t{
    struct sockaddr_in *p_addr; // Adresse du serveur
    
    int socket; // Socket du serveur
    int max_client; // Le nombre maximal de client que le serveur peut accepter
    int curSock; // Socket courante 
    int max_socket;

    int *clients; // Tableaux des clients connectés (sockets)
    p_flash_t flash; // Coeur de l'application Flash
} server_t;

/**
 * @brief Créer une instance du serveur
 * 
 * @param p_addr Adresse du serveur
 * @param id_socket_srv Id de la socket du serveur 
 * @param nb_connexion Nombre de connections max
 * @return server_t* Une instance du serveur
 */ 
server_t *create_server(struct sockaddr_in *p_addr, int id_socket_srv, int nb_connexion);

/**
 * @brief Mise en forme d'un message de debug
 * 
 * @param msg Message à afficher
 */
void debug(char *msg);
/**
 * @brief Établie la connexion
 * 
 * @param service Le port utilisé par la connexion
 * @param nb_connexion Nombre de connexion max
 */
void serveur (char *service,int nb_connexion);  
/**
 * @brief Lance le programme du serveur
 * 
 * @param server Instance du serveur
 */
void run();
/**
 * @brief Arrête le serveur
 * 
 * @param server L'instance du serveur
 * @param raison La raison de l'arrêt
 */
void arret();
/**
 * @brief Supprime un client de la liste
 * 
 * @param server l'instance du serveur
 * @param i L'index de la sockect
 */
void delete_client(int i);
/**
 * @brief Réalise l'action selon l'instruction reçue
 * 
 * @param server Instance du serveur
 * @param i Indexe de la sockect
 * @param buffer Buffer avec le message reçu
 */
void action(int i, char *buffer);
/**
 * @brief Envoie un post à un client
 * 
 * @param post Post à envoyer
 * @param user Le destinataire
 * @param flash L'instance du coeur de l'application
 */
void send_to(p_post_t post, p_user_t user, p_flash_t flash); 
/**
 * @brief Met à jour un utilisateur
 * 
 * @param flash Instance du coeur de l'applicaiton
 * @param socket Socket du client
 * @param pseudo pseudo de l'utilisateur à mettre à jour
 */
void update(p_flash_t flash, int socket, char *pseudo);

/**
 * @brief Nettoye le buffer
 * 
 * @param buffer Buffer à nettoyer
 * @return char* Le buffer nettoyé
 */
char *clear_buffer(char *buffer);
/**
 * @brief Nettoye et change la taille du buffer
 * 
 * @param buffer Le buffer à taiter
 * @param size La nouvelle taille
 * @return char* Le buffer traité
 */
char *new_buffer(char *buffer, size_t size);

/**
 * @brief Créer et ajoute un nouveau client
 * 
 * @param server Instance du serveur
 * @return int Id de la socket associée au nouveau client
 */
int new_client();

#endif //_SERVER_H_