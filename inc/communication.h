#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

// Liste des instructions possibles
#define I_LOGIN       "li\0"
#define I_LOGOUT      "lo\0"
#define I_SUBSCRIBE   "sc\0"
#define I_UNSUBSCRIBE "us\0"
#define I_PUBLISH     "pb\0"
#define I_LIST        "lt\0"

// Liste des réponses possibles
#define REP_OK    "0"
#define REP_ERROR "-1"

// Structure de données utilisée pour la communication
typedef struct {
    char cmd[3]; // Instruction envoyée
    char pseudo[11]; // Pseudo de l'utlisateur
    char dat[21]; // Données
} message_t;


#endif // _COMMUNICATION_H_