#ifndef _FLASH_H_
#define _FLASH_H_

#define PSEUDO_SIZE 10 // Taille du pseudp

/**
 * @brief Structure de données définisant un post
 * 
 */
typedef struct post_t{
    int cur_reader, max_reader; // Respectivement: indexe + 1 du dernier lecteur de la liste et taille de la liste de lecteur

    struct user_t *author; // Auteur du post
    struct user_t **readers; // Liste des lecteurs (ceux qui doivent lire le message)
    char *msg; // Le contenu du message
}post_t, *p_post_t;

/**
 * @brief Structure de données définisant un utilisateur
 * 
 */
typedef struct user_t{
    char *pseudo; // Pseudo de l'utilisateur
    int connection; // Socket de l'utilisateur
    int cur_post, cur_follower, cur_following; // Les index + 1 du dernier post, dernier follower, dernier abonnement
    int max_post, max_follower, max_following; // Taille des liste de posts, followers et d'abonnements

    struct user_t **followers; // Liste des followers
    struct user_t **following; // Liste des abonnements
    struct post_t **msg_sent; // Liste des posts réalisés
}user_t, *p_user_t;

/**
 * @brief Structure de données définisant le coeur de l'application
 * 
 */
typedef struct flash_t{
    p_user_t *users; // Liste des utilisateurs de l'applicaiton
    p_post_t *posts; // Liste des posts en attende d'être lu
    int cur_user, cur_post; // Indexes + 1 du dernier utilisateur et du dernier post
    int max_user,max_post; // Taille des listes des utilisateurs et des posts
}flash_t, *p_flash_t;

/**
 * @brief Créer et initialise le coeur de l'application
 * 
 * @return p_flash_t Instance du coeur
 */
p_flash_t create_flash();
/**
 * @brief Créer et ajout un utilisateur
 * 
 * @param socket Sockect associée à l'utilisateur
 * @param pseudo Pseudo choisi par l'utilisateur
 * @return p_user_t Le nouvel utilisateur
 */
p_user_t create_user(int socket, char *pseudo);
/**
 * @brief Récupère un utilisateur par son pseudo
 * 
 * @param flash Instance du coeur
 * @param pseudo Pseudo à chercher
 * @return p_user_t Null si aucun résultat, l'utilistaur correspondant sinon
 */
p_user_t get_by_pseudo(p_flash_t flash, char *pseudo);
/**
 * @brief Récupère la liste des abonnements d'un utilisateur
 * 
 * @param flash Instance du coeur
 * @param pseudo // Pseudo de l'utilisateur
 * @return p_user_t* Liste des abonnements
 */
p_user_t *get_subscriptions(p_flash_t flash, char *pseudo);

/**
 * @brief Permet de publier un post
 * 
 * @param flash Instance du coeur
 * @param pseudo Pseudo de l'auteur du post
 * @param msg  Message du psot
 * @return p_post_t Une instance du post
 */
p_post_t publish(p_flash_t flash, char *pseudo, char *msg);

/**
 * @brief Connextion d'un utilisateur
 * 
 * @param flash Instance du coeur
 * @param socket socket associée à l'utilisateur
 * @param pseudo Pseudo de l'utilisateur
 * @return int 0 si pas de problème, -1 sinon
 */
int login(p_flash_t flash, int socket, char *pseudo);
/**
 * @brief 
 * 
 * @param flash Instance du coeur
 * @param pseudo Pseudo de l'utilisateur à l'origine de la commande
 * @param following Pseudo de l'utilisateur à suivre
 * @return int 0 si pas de problème, -1 sinon
 */
int subscribe(p_flash_t flash, char *pseudo, char *following);
/**
 * @brief 
 * 
 * @param flash Instance du coeur
 * @param pseudo Pseudo de l'utilisateur à l'origine de la commande
 * @param following Pseudo de l'utilisatuer à supprimer des abonnements
 * @return int 0 si pas de problème, -1 sinon
 */
int unsubscribe(p_flash_t flash, char *pseudo, char *following);
/**
 * @brief Déconnecte un utilisateur
 * 
 * @param flash Instance du coeur
 * @param pseudo Pseudo de l'utilisateur
 * @return int 0 si pas de problème, -1 sinon
 */
int logout(p_flash_t flash, char *pseudo);

/**
 * @brief Supprime un lecteur d'un post
 * 
 * @param p Post à mettre à jour
 * @param u Lecteur à supprimer
 */
void remove_reader(p_post_t p, p_user_t u);
/**
 * @brief Supprime un post de la liste d'attente
 * 
 * @param f Instance du coeur
 * @param p Post à supprimer
 */
void remove_post(p_flash_t f, p_post_t p);
/**
 * @brief Destructeur de la structure flash (libère la mémoire)
 * 
 * @param f Instance du coeur
 */
void destruct_flash(p_flash_t f);
/**
 * @brief Destructeur de la structure user (libère la mémoire)
 * 
 * @param u Instance d'un utilisateur
 */
void destruct_user(p_user_t u);
/**
 * @brief Destructeur de la structure post (libère la mémoire)
 * 
 * @param p Instance d'un post
 */
void destruct_post(p_post_t p);

#endif //_FLASH_H_
