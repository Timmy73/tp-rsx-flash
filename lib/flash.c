#include "flash.h"
#include "fon.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

p_flash_t create_flash(){
    p_flash_t instance = malloc(sizeof(flash_t));
    
    instance->max_user = 1; instance->max_post = 1;
    instance->cur_user = 0; instance->cur_post = 0;

    instance->users    = malloc(sizeof(p_user_t) * instance->max_user);
    instance->posts    = malloc(sizeof(p_user_t) * instance->max_post);
    return instance;
}

p_user_t create_user(int socket, char *pseudo){
    p_user_t user = malloc(sizeof(user_t));

    user->pseudo = malloc(sizeof(char) * PSEUDO_SIZE + 1);
    strcpy(user->pseudo,pseudo);
    
    user->connection = socket;
    user->cur_post   = 0; user->cur_follower = 0; user->cur_following = 0;
    user->max_post   = 1; user->max_follower = 1; user->max_following = 1;

    user->followers  = malloc(sizeof(p_user_t) * user->max_follower);
    user->following  = malloc(sizeof(p_user_t) * user->max_following);
    user->msg_sent   = malloc(sizeof(p_post_t) * user->max_post);

    return user;
}

p_user_t get_by_pseudo(p_flash_t flash, char *pseudo){
    p_user_t user = NULL;
    register int i = 0;

    while(i < flash->cur_user && user == NULL){
        if(strcmp(flash->users[i]->pseudo, pseudo) == 0) user = flash->users[i]; 
        i++;
    }

    return user;
}

p_user_t *get_subscriptions(p_flash_t flash, char *pseudo){
    p_user_t user = get_by_pseudo(flash, pseudo);
    return user ? user->following : NULL;
}

p_post_t publish(p_flash_t flash, char *pseudo, char *msg){
    p_user_t user = get_by_pseudo(flash, pseudo);
    
    if(!user) return NULL;
    if(strlen(msg) > 20) return NULL;
    
    p_post_t post    = malloc(sizeof(post_t));
    post->readers    = malloc(sizeof(p_user_t) * user->cur_follower);
    post->msg        = malloc(sizeof(char) * strlen(msg));
    post->cur_reader = user->cur_follower;

    post->author  = user;
    strcpy(post->msg, msg);

    for(register int i = 0; i < user->cur_follower; i++){
        post->readers[i] = user->followers[i];
    }

    if(flash->cur_post >= flash->max_post){
        flash->max_post *= 2;
        flash->posts = realloc(flash->posts, sizeof(p_post_t) * flash->max_post);
    }

    flash->posts[flash->cur_post] = post;
    flash->cur_post++;

    return post;
}

int login(p_flash_t flash, int socket, char *pseudo){
        for(int i = 0; i < flash->cur_user; i++){
            printf("\t>>%s\n",flash->users[i]->pseudo);
        }
    p_user_t user = get_by_pseudo(flash, pseudo);
    
    if(user) user->connection = socket;
    else{
        if(strlen(pseudo) > 10) return -1;

        if(flash->cur_user >= flash->max_user){
            flash->max_user *= 2;
            flash->users = realloc(flash->users, sizeof(p_user_t) * flash->max_user);
        }

        flash->users[flash->cur_user] = create_user(socket, pseudo);
        flash->cur_user++;

    }

    return 0;
}

int subscribe(p_flash_t flash, char *pseudo, char *following){
    p_user_t user      = get_by_pseudo(flash, pseudo);
    p_user_t followed  = get_by_pseudo(flash, following);
    
    if(!user || !following) return -1;
    
    if(user->cur_following >= user->max_following){
        user->max_following *= 2;
        user->following = realloc(user->following, sizeof(p_user_t) * user->max_following);
    }

    if(followed->cur_follower >= followed->max_follower){
        followed->max_follower *= 2;
        followed->followers = realloc(followed->followers, sizeof(p_user_t) * followed->max_follower);
    }

    user->following[user->cur_following] = followed;
    user->cur_following++;

    followed->followers[followed->cur_follower] = user;
    followed->cur_follower++;

    return 0;
}

int unsubscribe(p_flash_t flash, char *pseudo, char *following){
    p_user_t user      = get_by_pseudo(flash, pseudo);
    p_user_t followed  = get_by_pseudo(flash, following);
    int index_user_as_follower = -1;
    int index_followed = -1;

    if(!user || !following) return -1;

    for(register int i = 0; i < user->cur_following && index_followed == -1; i++){
        if(strcmp(followed->pseudo, user->following[i]->pseudo) == 0) 
            index_followed = i;
    }

    for(register int i = 0; i < followed->cur_follower && index_user_as_follower == -1; i++){
        if(strcmp(user->pseudo,followed->followers[i]->pseudo) == 0)
            index_user_as_follower = i;
    }

    if(index_user_as_follower < 0 || index_followed < 0) return -1;

    user->following[index_followed] = NULL;
    followed->followers[index_user_as_follower] = NULL;

    for(register int i = index_followed + 1; i < user->cur_following; i++){
        user->following[i - 1] = user->following[i];
    }
    user->following[user->cur_following - 1] = NULL;
    user->cur_following--;

    for(register int i = index_user_as_follower + 1; i < followed->cur_follower; i++){
        followed->followers[i - 1] = followed->followers[i]; 
    }
    followed->followers[followed->cur_follower - 1] = NULL;
    followed->cur_follower--;

    return 0;
}

int logout(p_flash_t flash, char *pseudo){
    p_user_t user = get_by_pseudo(flash,pseudo);
    if(!user) return -1;
    user->connection = -1;
    return 0;
}

void remove_reader(p_post_t p, p_user_t u){
    register int i = 0;
    for(; i < p->cur_reader; i++){
        if(strcmp(p->readers[i]->pseudo, u->pseudo) == 0) break;
    }

    if(i == p->cur_reader) return;
    
    p->readers[i] = NULL;
    for(register int j = i + 1; j < p->cur_reader; j++){
        p->readers[j - 1] = p->readers[j];
    }
    p->readers[p->cur_reader - 1] = NULL;
    p->cur_reader--;
}

void remove_post(p_flash_t f, p_post_t p){
    register int i = 0;
    for(; i < f->cur_post; i++){
        if(f->posts[i] == p) break;
    }

    f->posts[i] = NULL;
    for(register int j = i + 1; j < f->cur_post; j++){
        f->posts[j - 1] = f->posts[j];
    }
    f->posts[f->cur_post - 1] = NULL;
    f->cur_post--;
}

void destruct_flash(p_flash_t f){
    register int i = 0;

    while(i < f->cur_user){
        destruct_user(f->users[i]);
        i++;
    }
    i = 0;
    while(i < f->cur_post){
        destruct_post(f->posts[i]);
        i++;
    }

    free(f->users);
    free(f->posts);
    free(f);
}

void destruct_user(p_user_t u){
    h_shutdown(u->connection, FIN_ECHANGES);
    h_close(u->connection);
    
    free(u->followers);
    free(u->following);
    free(u->msg_sent);
    free(u);
}

void destruct_post(p_post_t p){
    free(p->readers);
    free(p);
}