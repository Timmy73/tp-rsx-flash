#include "inc/server.h"
#include <string.h>
#include <signal.h>

#define BUFFER_SIZE 1024

server_t *inst; // Intance de la structure server pour faciliter la gestion

int main(int argc, char **argv){

	char *service    = SERVICE_DEFAUT; /* numero de service par defaut */
	int nb_connexion = NB_CONNEXION_DEFAUT; // Nombre de connexion simultanées autorisées
    
    signal(SIGINT,arret); // Pour arrêter le server "proprement" lors d'un ctrl+c

    switch (argc) 
 	{
        case 1:
            printf("default service = %s\n", service);
            break;
        case 2:
            service = argv[1];
            break;
        case 3:
            service      = argv[1];
            nb_connexion = atoi(argv[2]);
        default :
            printf("Usage:serveur service (nom ou port)  nb_de_connexions_possibles\n");
            exit(1);
 	}
    serveur(service, nb_connexion);

    return 0;
}

server_t *create_server(struct sockaddr_in *p_addr, int id_socket_server, int nb_connexion){
    inst = malloc(sizeof(server_t));
    
    inst->p_addr     = p_addr;
    inst->socket     = id_socket_server;
    inst->max_client = nb_connexion;
    inst->curSock    = 0;
    inst->clients    = malloc(sizeof(int) * nb_connexion);
    inst->flash      = create_flash();
    inst->max_socket = id_socket_server;

    return inst;
}

void serveur (char *service,int nb_connexion){
    struct sockaddr_in *p_addr;

    int id_socket_server = h_socket(AF_INET, SOCK_STREAM); // Création de la socket TCP
	
    if(id_socket_server < 0) {
		printf("ERREUR: Demarrage impossible.\n");
		exit(-1);
	}

	adr_socket(service, "127.0.0.1", SOCK_STREAM, &p_addr);
    h_bind(id_socket_server, p_addr); // Bind de la socket passive
	h_listen(id_socket_server,nb_connexion); // Mise en action de la socket passive
    
    inst = create_server(p_addr, id_socket_server, nb_connexion); // Création de l'instance server

    for(int i = 0; i < nb_connexion; i++){ // Initialisation des sockets clients
        inst->clients[i] = -1;
    }

    run(); // Lancement serveur
}

void run(){
    
    char *buffer = malloc(sizeof(message_t)); // Création du buffer
    fd_set read; // fd_set pour savoir quelle socket nous traitons

    while(1){
        debug("Restart");
        FD_ZERO(&read); // Initialisation du fd_set
        FD_SET(inst->socket,&read);

        for(int i = 0; i < inst->curSock; i++){ // Ajout des connexions clientes au fd_set
            FD_SET(inst->clients[i], &read);
        }

        if(select(inst->max_socket + 1, &read, NULL, NULL, NULL) < 0){
            perror("Error select"); exit(errno);
        }

        debug("Select get socket");
        
        if(FD_ISSET(inst->socket, &read)) new_client(); // Si c'est la socket du server alors c'est un nouveau client
        else{ // Sinon on traite le paquet reçu
            for(int i = 0 ; i < inst->curSock; i++){ 
                debug("Searching socket...");
                if(FD_ISSET(inst->clients[i], &read)){
                    debug("Socket found");
                    debug("Reading");
                    if(h_reads(inst->clients[i], buffer, sizeof(message_t)) > 0){
                        debug("READ");
                        action(i, buffer);
                    }else delete_client(i);
                }
            }
        }
    }
}

void arret(){
	h_shutdown(inst->socket,FIN_ECHANGES);
	h_close(inst->socket);

    free(inst->clients);
    destruct_flash(inst->flash);
    free(inst);
    printf("Good Bye!\n");
}

void debug(char *msg){
     printf("DEBUG>> %s\n",msg);
}

void delete_client(int i){ // Suppression d'un client
    h_shutdown(inst->clients[i],FIN_ECHANGES);
    h_close(inst->clients[i]);
    
    inst->clients[i] = -1; // Suppression client
    
    // Décalage de tous les clients après celui qui vient d'être supprimé
    for(register int j = 1; j < inst->curSock; j++){
        inst->clients[j - 1] = inst->clients[j];
    }

    inst->clients[inst->curSock] = -1;
    inst->curSock--;

    // MAJ du max du FD_SET
    int max = 0;
    for(register int j = 0; j < inst->curSock; j++){
        max = max < inst->clients[j] ? inst->clients[j] : max; 
    }

    inst->max_socket = max;

    debug("Socket removed");
}

void action(int i, char *buffer){
    message_t *req  = malloc(sizeof(message_t));
    message_t *res  = malloc(sizeof(message_t));
    p_flash_t flash = inst->flash;
    p_post_t  post  = NULL;

    memcpy((void *)req, (void *)buffer, sizeof(message_t));

    debug("Msg received:");
    printf("\t[%s]%s|%s.\n",req->pseudo,req->cmd,req->dat);

    // On regarde quelle commande a été envoyer par le client
    if(strcmp(req->cmd, I_LOGIN) == 0){
        if(login(inst->flash, inst->clients[i], req->pseudo) == 0){
            strcpy(res->cmd, I_LOGIN);
            strcpy(res->dat, "Welcome!");

            debug("List of users updated: ");
            for(int j = 0; j < inst->flash->cur_user; j++){
                if(inst->flash->users[j]) printf("\t-%s\n",inst->flash->users[j]->pseudo);
            }

            debug("New client!");
        }else strcpy(res->cmd, REP_ERROR);

    }else if(strcmp(req->cmd, I_LOGOUT) == 0){

        if(logout(flash, req->pseudo) == 0){
            strcpy(res->cmd, I_LOGOUT);
            strcpy(res->dat, "Good Bye! :(");
            debug("Logout");
        }else strcpy(res->cmd, REP_ERROR);

    }else if(strcmp(req->cmd, I_SUBSCRIBE) == 0){

        if(subscribe(flash, req->pseudo, req->dat) == 0){
            strcpy(res->cmd, I_SUBSCRIBE);
            strcpy(res->dat, "Followed");
            debug("Subscribers");
            p_user_t usr = get_by_pseudo(flash, req->dat);
            if(usr){
                for(int i = 0; i < usr->cur_follower; i++)
                    printf("\t-%s\n",usr->followers[i]->pseudo);
            }
        }else strcpy(res->cmd, REP_ERROR);

    }else if(strcmp(req->cmd, I_UNSUBSCRIBE) == 0){

        if(unsubscribe(flash, req->pseudo, req->dat) == 0){
            strcpy(res->cmd, I_UNSUBSCRIBE);
            strcpy(res->dat, "Stop following");
            debug("Unsubscribe");
        }else strcpy(res->cmd, REP_ERROR);

    }else if(strcmp(req->cmd, I_PUBLISH) == 0){
        if((post = publish(flash, req->pseudo, req->dat))){
            strcpy(res->cmd, I_PUBLISH);
            strcpy(res->dat, "Message posted");
            debug("Message Posted");

            debug("Followers list: ");
            for(register int i = 0; i < post->cur_reader; i++){
                printf("\t-%s\n",post->readers[i]->pseudo);
            }

            for(register int i = 0; i < post->cur_reader; i++){
                send_to(post, post->readers[i], flash);
            }

            if(post){
                free(post);
            }
            
        }else strcpy(res->cmd, REP_ERROR);

    }else if(strcmp(req->cmd, I_LIST) == 0){
        p_user_t user;
        
        if((user = get_by_pseudo(flash, req->pseudo))){
            for(register int i = 0; i < user->cur_following; i++){
                message_t *msg = malloc(sizeof(message_t));
                strcpy(msg->cmd, I_LIST);
                strcpy(msg->dat, user->following[i]->pseudo);

                buffer = new_buffer(buffer, sizeof(message_t));
                memcpy((void *)buffer, (void *)msg, sizeof(message_t));
                h_writes(inst->clients[i], buffer, sizeof(message_t));
            }
        }
        return;
    }
    
    debug("Response:");
    printf("\t[%s]%s|%s.\n",res->pseudo,res->cmd,res->dat);
    
    buffer = new_buffer(buffer, sizeof(message_t));
    memcpy((void *)buffer, (void *)res, sizeof(message_t));
    
    h_writes(inst->clients[i], buffer, sizeof(message_t));
    update(flash, inst->clients[i], req->pseudo);
    
    free(req); free(res);
    
    debug("End Action");
}

void send_to(p_post_t post, p_user_t user, p_flash_t flash){
    if(user->connection <= 0) return;

    char      *buffer  = malloc(sizeof(message_t));
    message_t *message = malloc(sizeof(message_t));

    strcpy(message->cmd, REP_OK);
    strcpy(message->pseudo, post->author->pseudo);
    strcpy(message->dat,post->msg);

    memcpy((void *)buffer, (void *)message, sizeof(message_t));
    h_writes(user->connection, buffer, sizeof(message_t));
    remove_reader(post, user);
    if(post->cur_reader == 0) remove_post(flash, post);

    free(buffer);
    free(message);
}

void update(p_flash_t flash, int socket, char *pseudo){
    p_user_t user;
    
    if(!(user = get_by_pseudo(flash, pseudo))) return;
    for(register int i = 0; i < flash->cur_post; i++){
        p_post_t post = flash->posts[i];
        
        register int j = 0;
        for(; j < post->cur_reader && post->readers[j]; j++);
        if(j < post->cur_reader) send_to(post, user, flash);
    }
}

char *clear_buffer(char *buffer){
	free(buffer);
	return malloc(sizeof(char) * BUFFER_SIZE);
}

char *new_buffer(char *buffer, size_t size){
    free(buffer);
    return malloc(size);
}

int new_client(){
    int socket = h_accept(inst->socket, inst->p_addr);

    if(inst->curSock < inst->max_client){
        inst->clients[inst->curSock] = socket;
        inst->curSock++;
        
        debug("Socket added");
        printf("\told %i sock %i\n", inst->max_socket, socket);
        inst->max_socket = inst->max_socket < socket ? socket : inst->max_socket; 
        printf("\tNew %i \n", inst->max_socket);
    }else{
        perror("\tCan not add socket");
        exit(-1);
    }

    return socket;
}
