
OBJ1 = client.o
OBJ2 = server.o

H_PATH=include

LIB_PATH=lib/
LIB_SRC=$(wildcard $(LIB_PATH)*.c)
LIB=$(patsubst %.c, $(LIB_PATH)%.o, $(notdir $(LIB_SRC)))

OPTIONS	=
LFLAGS += -g
# Adaptation a Darwin / MacOS X avec fink
# Du fait de l'absence de libtermcap on se fait pas mal
# cracher dessus mais ca marche...
ifeq ($(shell uname), Darwin)
LFLAGS	+= -L/opt/local/lib
CFLAGS	+= -I /opt/local/include
endif
#Changer si necessaire le chemin d'acces aux librairies

# Adaptation a Linux
ifeq ($(shell uname), Linux)
OPTIONS	+= -ltermcap
endif

# Adaptation a FreeBSD
# Attention : il faut utiliser gmake...
ifeq ($(shell uname),FreeBSD)
OPTIONS	+= -ltermcap
endif

# Adaptation a Solaris

ifeq ($(shell uname),SunOS)
OPTIONS	+= -ltermcap  -lsocket -lnsl
CFLAGS	+= -I..
endif

CFLAGS += -I$(H_PATH)
EXEC = client server

all: ${EXEC} 	

$(LIB_PATH)%.o: $(LIB_PATH)%.c $(LIB_PATH)%.h  
	gcc -g -o $@ -c $<

%.o: %.c %.h
	gcc -g -Wall -Werror -c $< -o $@

client : ${OBJ1} ${LIB}
	gcc $(LFLAGS) $^ -g -o $@  $(OPTIONS)

server : ${OBJ2} ${LIB}	
	gcc  $(LFLAGS) $^ -g -o $@  $(OPTIONS)

clean : 
	rm -f ${EXEC} *.o $(LIB) test

test : test.c ${LIB}
	gcc $(LFLAGS) $^ -g -o $@

