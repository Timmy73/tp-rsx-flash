#include "inc/client.h"

#include <string.h>

int main(int argc, char **argv){

    char *server  = SERVER_DEFAULT;
    char *service = SERVICE_DEFAULT;
    
    switch (argc)
	{
        case 1:
            printf("serveur par defaut: %s\n", server);
            printf("service par defaut: %s\n", service);
            break;
        case 2:
            server = argv[1];
            printf("service par defaut: %s\n", service);
            break;
        case 3:
            server = argv[1];
            service = argv[2];
            break;
        default:
            printf("Usage:client serveur(nom ou @IP)  service (nom ou port) \n");
            exit(1);
	}

    start(server, service);

    return 0;
}

void start(char *server, char *service){
    struct sockaddr_in *p_addr;
    
    int    socket = h_socket(AF_INET, SOCK_STREAM);
    size_t size   = PSEUDO_SIZE;
    
    char *buffer = malloc(sizeof(message_t));
    char *pseudo = NULL;

    // Création de la socket client
    adr_socket(service, server, SOCK_STREAM, &p_addr);
    h_connect(socket, p_addr);

    printf("Biencenue dans Flash ! \n");
    printf("Merci de saisir un pseudo : ");
    getline(&pseudo, &size, stdin);
    pseudo[strlen(pseudo) - 1] = '\0';
    
    // Préparation de l'envoi d'un paquet au serveur
    message_t *msg = malloc(sizeof(message_t));
    strcpy(msg->cmd, I_LOGIN);
    strcpy(msg->pseudo, pseudo);
    
    // Envoi du paquet
    memcpy((void *) buffer, (void *) msg, sizeof(message_t));
    h_writes(socket, buffer, sizeof(message_t));
    
    // Si réponse du serveur
    if(h_reads(socket, buffer, sizeof(message_t))> 0){
        
        memcpy((void *)msg, (void *) buffer, sizeof(message_t));
        
        if(strcmp(msg->cmd,REP_ERROR) == 0){
            h_shutdown(socket, FIN_RECEPTION);	h_close(socket);
            free(buffer); free(pseudo); free(msg);
            perror("Erreur de login \n"); exit(-1);
        }else{
            printf("\n\nSERVER>> %s\n",msg->dat);
        }
        run(socket, pseudo);
    }
    free(buffer); free(msg); free(pseudo);
    
}

// Affichage de l'aide
void print_help(){
    printf("*** AIDE ***\n");
    printf("\t %s <login> : S'abonner à un utilisateur\n",I_SUBSCRIBE);
    printf("\t %s <login> : Se désabonner à un utilisateur\n",I_UNSUBSCRIBE);
    printf("\t %s <message> : Publier un message\n",I_PUBLISH);
    printf("\t %s : Lister tous vos abonnements\n",I_LIST);
    printf("\t h : Afficher l'aide\n");
    printf("\t q : Quitter l'application \n");
    printf("************\n");

    printf("\n Appuyez sur Enter pour continuer.\n");
}

void run(int socket, char *pseudo){
    char *buffer = malloc(sizeof(message_t)); 
    printf("\n\n*** Vous êtes connecté ***\n\n");
    print_help();
    
    while(strcmp(buffer,"q") != 0){

        int max = socket;
        fd_set read;
        
        FD_ZERO(&read);
        FD_SET(socket, &read);

        // On ajoute l'entrée standart dans le FD_SET pour faciliter la gestion de la socket et de l'entrée standart
        FD_SET(STDIN_FILENO, &read); 

        if(select(max + 1, &read, NULL, NULL, NULL) <= 0) printf("Erreur\n");
        else{
            if(FD_ISSET(STDIN_FILENO, &read)){ 
                // Si c'est l'entrée standat on récupère ce que tape l'utilisateur et on l'envoie au serveur
                clear_stdin();
                printf("(%s@flsah)$ ",pseudo);
                fgets(buffer, BUFFER_SIZE, stdin);

                if(buffer[strlen(buffer) - 1] == '\n') buffer[strlen(buffer) - 1] = '\0';
                
                send_server(socket, buffer, pseudo);
            }

            if(FD_ISSET(socket, &read)){ // Si c'est la socket on lit son contenu
                if(h_reads(socket, buffer, sizeof(message_t)) > 0){
                    display(socket, buffer);
                }
            }
        }
    }
    free(buffer);
}

void clear_stdin(){
    char c;
    while((c = getchar()) != '\n' && c != EOF);
}

void display(int socket, char *buffer){ // Mise en page du message postée
    message_t *msg = malloc(sizeof(message_t));

    memcpy((void *)msg, (void *)buffer, sizeof(message_t));

    if(strcmp(msg->cmd, REP_ERROR) == 0){
        printf("Erreur sur le serveur \n");
    }else if(strcmp(msg->cmd, I_PUBLISH) == 0){
        printf("----------- MESSAGE DE %s -----------\n", msg->pseudo);
        printf("\t* %s\n", msg->dat);
        printf("-------------------------------------\n");

    }else{
        printf("\t* [%s] %s\n", msg->cmd, msg->dat);
    }

    free(msg);
}

// Génération du paquet à envoyer au serveur avec les données nécessaires
void send_server(int socket, char *line, char *pseudo){
    char *buffer = malloc(sizeof(message_t));

    // Pour savoir si la commande tapée prend des paramètres
    char *cmd    = strlen(line) > 1 ? substr(line, 0, 2) : line; 
    char *data   = strlen(line) > 3 ? substr(line, 3, strlen(line)) : NULL;

    printf(">>CMD: %s\n",cmd);
    printf(">>DATA: %s,\n",data);

    message_t *msg = malloc(sizeof(msg));

    if(strcmp(cmd, I_SUBSCRIBE) == 0){
        strcpy(msg->cmd,I_SUBSCRIBE);
        strcpy(msg->dat, data);
    }else if(strcmp(cmd, I_UNSUBSCRIBE) == 0){
        strcpy(msg->cmd,I_UNSUBSCRIBE);
        strcpy(msg->dat, data);
    }else if(strcmp(cmd, I_PUBLISH) == 0){
        strcpy(msg->cmd,I_PUBLISH);
        strcpy(msg->dat, data);
    }else if(strcmp(cmd, I_LIST) == 0){
        strcpy(msg->cmd,I_LIST);
    }else if(strcmp(cmd, "h") == 0){
        print_help();
        free(msg); free(buffer);
        return;
    }else if(strcmp(cmd, "q") == 0){
        strcpy(msg->cmd, I_LOGOUT);
    }else{
        perror("Erreur, mauvaise commande\n");
        print_help();
        free(msg); free(buffer);
        return;
    }
    strcpy(msg->pseudo,pseudo);

    memcpy((void *)buffer, (void *)msg, sizeof(message_t));
    h_writes(socket, buffer, sizeof(message_t));
    
    free(buffer); free(msg);
    if(strlen(line) > 2){
        free(cmd); free(data);
    }
}

// Découpe la chaine string en partant de l'index begin à begin + length
char *substr(char *string, int begin, int length){
    char *res = malloc(sizeof(char) * length);
    for(register int i = begin; i < begin + length; i++){
        res[i - begin] = string[i];
    }
    return res;//
}